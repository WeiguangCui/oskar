===============================================================================
Module: FindOpenMP.cmake
Version: cmake 3.5.1 module 
Depends on:
    CMakeParseArguments.cmake
    CheckCSourceCompiles.cmake
    CheckCXXSourceCompiles.cmake
    FindPackageHandleStandardArgs.cmake
    FindPackageMessage.cmake

Added options to work with homebrew clang-omp and homebrew clang-3.8 
(versions/llvm38)
===============================================================================
