classdef type < uint32
    enumeration
        circular (0)
        spiralArchimedean (1)
        archimedean (1)
        spiralLog (2)
        log (2)
    end
end
