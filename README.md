# OSKAR: A GPU accelerated simulator for the Square Kilometre Array

OSKAR has been designed to produce simulated visibility data from radio 
telescopes containing aperture arrays, such as those envisaged for the 
Square Kilometre Array.

In order to run OSKAR, you will need a CUDA-capable NVIDIA graphics 
processing unit (GPU). Please see INSTALL.md for a quick installation guide.

Full documentation and software can be found at http://oskar.oerc.ox.ac.uk

**Important!** Do not expect code in any development branch 
(including master) to work as expected.
Only use the tagged versions from http://oskar.oerc.ox.ac.uk

Please email oskar@oerc.ox.ac.uk if you encounter problems using OSKAR.

OSKAR is licensed under the terms of the Modified BSD License.
Please see COPYING.md for details. 

